<?php
namespace App\Shell;

use Cake\Console\Shell;
use App\Controller\CrawlController;
class CrawlShell extends Shell
{
    public function main()
    {
        $hm = new CrawlController();
        // run it and commitn crawlData for the first time to get ting list actor.
        // $hm->index();
        $hm->crawlData();
    }
}
