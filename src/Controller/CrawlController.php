<?php

namespace App\Controller;

use App\Controller\AppController;
use Aura\Intl\Exception;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;

class CrawlController extends AppController
{

    protected $tblLinks;
    protected $tblActors;
    protected $tblFilms;
    protected $crawlBaseUrl;
    protected $tblActorFilms;

    public function initDefault()
    {
        $this->autoRender = false;
        require APP .  "Lib/simplehtmldom_1_5/simple_html_dom.php";
        $this->crawlBaseUrl = "http://asianwiki.com/";
        $this->tblLinks = TableRegistry::get('Links');
        $this->tblActors = TableRegistry::get('Actors');
        $this->tblFilms = TableRegistry::get('Films');
        $this->tblActorFilms = TableRegistry::get('ActorsFilms');

        $this->crawlBaseUrl = "http://asianwiki.com";
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "1024M");

        // test section
//        $this->test();


    }

    protected function test()
    {
        $this->crawlActors();
        $this->crawlFilms();
        exit('end crawl films');
    }

    public function index()
    {
	$this->initDefault();
        $this->crawlParentLinks();
        $parentLinks = $this->tblLinks->getParentLinks();
        if (!$parentLinks) {
            echo 'there are no parent links -- please run crawl/getparentlink to get more links';
        }
        $count = 0;
        foreach ($parentLinks as $parentLinkID => $parentLinkName) {
            $parentHtml = @file_get_html($parentLinkName);
            if ($this->isCategoryPageEmpty($parentHtml) || !$parentHtml) {
                $this->tblLinks->updateCrawledForLink($parentLinkID);
                continue;
            }
            $isHaveData = true;
            $currentCrawlerLink = $parentLinkName;
            $currentCategoryId = $parentLinkID;
            // check gender
            $formatLink = strtolower($parentLinkName);
            $gender = 1; // men
            if (strpos($formatLink, 'actress') !== false) {
                $gender = 2;
            }
            do {

                $domHtml = @file_get_html($currentCrawlerLink);
                if (!$domHtml || ($currentCrawlerLink != $parentLinkName && $this->isCategoryPageEmpty($domHtml))) {
                    // currenty link category have no content
                    $this->updateCrawledForLink($currentCategoryId);
                    continue;
                }
                $listLinks = $domHtml->find('#mw-pages a');
                foreach ($listLinks as $key => $value) {
                    if (!isset($value->href)) {
                        continue;
                    }
                    $link = $value->href;
                    $fullLink = $this->getFulUrlCrawlLink($link);
                    if ($this->isCategoryLink($link)) {

                        if (!$this->isCategoryLinkExist(['link' => $fullLink])) {
                            // insert into links table
                            $this->tblLinks->saveLink(['link' => $fullLink, 'parent_id' => $parentLinkID]);
//                            echo $fullLink . '<br>';
                            $count++;
                        }
                    } else {
                        // normal link(actor)
                        $actor_key_name = $this->filterActorName($link);
                        $actor_name = trim($value->plaintext);

                        if (!$this->isActorExist(['key_name' => $actor_key_name])) {
                            $this->tblActors->saveActor(['key_name' => $actor_key_name, 'name' => $actor_name, 'gender' => $gender]);
                        }

                    }
                }
                $this->tblLinks->updateCrawledForLink($currentCategoryId);
                $childCategory = $this->tblLinks->getChildCategoryLinkNotActiveByParent($parentLinkID);
                if (!$childCategory) {
                    $isHaveData = false;
                } else {
                    $currentCategoryId = $childCategory['id'];
                    $currentCrawlerLink = $childCategory['link'];
                }
//                if ($count == 5) {
//                    exit('stop at here');
//                    break;
//                }
            } while ($isHaveData != false);
            $this->tblLinks->updateCrawledForLink($parentLinkID);
        }
        // crawl actors
        $this->crawlData();

    }

    public function crawlData()
    {
	$this->initDefault();
        $remainActors = true;
        $remainFilms = true;
        do {
            $remainActors = $this->crawlActors();
            $remainFilms = $this->crawlFilms();

            if (!$remainFilms && !$remainActors) {
                break;
            }
        } while ($remainActors && $remainFilms);
    }

    protected function crawlActors()
    {
        $objActors = $this->tblActors->getActorsNotCrawled(1000);
        if (!$objActors->count()) {
            return false;
        }
        foreach ($objActors as $actor) {
            $url = $this->crawlBaseUrl .'/'. $actor['key_name'];
//            $url = "http://asianwiki.com/Akiko_Aitsuki";
//            $actor['id'] = 83;
            $actorID = $actor['id'];
            $actorHtml = @file_get_html($url);
            if (!$actorHtml) {
                $this->tblActors->updateCrawlStatus($actor['id'], 1);
                continue;
            }
            // crawl profile
            $profileInfo = array();
            $moviesInfo = array();
            $tvSeriesInfo = array();
            $dramaSeriesInfo = array();
            $avatar_actor = "";
            $awardInfo = array();
            $filmographyInfo = array();
            $biographyInfo = array();
            $noteInfo = "";
                if (isset($actorHtml->find('#Profile', 0)->plaintext)) {
                    $profileInfo = $this->getProfileFromCrawler($actorHtml);
                }

            // get note
            if (isset($actorHtml->find('#Notes', 0)->plaintext)) {
                $noteHtml = $actorHtml->find('#Notes', 0)->parent()->next_sibling();
                if ($noteHtml->tag == 'ol' || $noteHtml->tag == 'ul') {
                    // removed tag a if exists
                    $noteInfo = strip_tags($noteHtml->outertext, '<ol>,<ul>,<li>,<b>,<p>');
                }
            }
            // crawl Biography
            // get plot
            if (isset($actorHtml->find('#Biography', 0)->plaintext)) {
                $biographyHtml = $actorHtml->find('#Biography', 0)->parent()->next_sibling();
                if ($biographyHtml->tag == 'p') {
                    $isHaveBiography = true;
                    do {
                        $biographyInfo[] = strip_tags($biographyHtml->outertext,'<ol>,<ul>,<li>,<b>,<p>');

                        $biographyHtml = $biographyHtml->next_sibling();
                        if ($biographyHtml->tag != 'p') {
                            $isHaveBiography = false;
                        }
                    } while ($isHaveBiography != false);
                }
            }
                // crawl films
                if (isset($actorHtml->find('#Movies', 0)->plaintext)) {
                    $moviesInfo = $this->getFilmFromCrawlerActorPage($actorHtml, '#Movies');
                }

                // crawl Drama_Series
                if (isset($actorHtml->find('#Drama_Series', 0)->plaintext)) {
                    $dramaSeriesInfo = $this->getFilmFromCrawlerActorPage($actorHtml, '#Drama_Series');
                }
                // crawl TV_Movies
                if (isset($actorHtml->find('#TV_Movies', 0)->plaintext)) {
                    $tvSeriesInfo = $this->getFilmFromCrawlerActorPage($actorHtml, '#TV_Movies');
                }

                // crawl Filmography

                if (isset($actorHtml->find('#Filmography', 0)->plaintext)) {
                    $filmographyInfo = $this->getFilmFromCrawlerActorPage($actorHtml, '#Filmography');
                }
                // crawl avatars

                if (isset($actorHtml->find('#mw-content-text .thumbinner img.thumbimage', 0)->src)) {
                    $avatar_actor = $actorHtml->find('#mw-content-text .thumbinner img.thumbimage', 0)->src;
                    if (strpos($avatar_actor, 'asianwiki.com') === false) {
                        $avatar_actor = $this->crawlBaseUrl . $avatar_actor;
                    }
                }

                // crawl Awards
                if (isset($actorHtml->find('#Awards', 0)->plaintext)) {
                    $awardHtml = $actorHtml->find('#Awards', 0)->parent()->next_sibling();
                    if ($awardHtml->tag == 'ul') {
                        $awardInfo = strip_tags($awardHtml->outertext, '<ol>,<ul>,<li>,<b>');
                    }
                }

                $data['avatar'] = $avatar_actor;
                $data['drama_series'] = $dramaSeriesInfo;
                $data['tv_movies'] = $tvSeriesInfo;
                $data['movies'] = $moviesInfo;
                $data['filmography'] = $filmographyInfo;
                $data['award'] = $awardInfo;
                $data['profile'] = $profileInfo;
                $data['note'] = $noteInfo;
                $data['biography'] = $biographyInfo;
                $this->saveDataFromActorCrawler($actorID, $data);
                $this->tblActors->updateCrawlStatus($actor['id'], 1);
	return true;
        }

    }

    protected function crawlFilms()
    {
        $objDbFilms = $this->tblFilms->getFilmsNotCrawled(300);
        if (!$objDbFilms->count()) {
            return false;
        }

        foreach ($objDbFilms as $dbFilm) {
            $keyName = $dbFilm['key_name'];
            $crawUrl = $this->crawlBaseUrl . '/' . $keyName;
            $dbFilmID = $dbFilm['id'];
//            $crawUrl = 'http://asianwiki.com/Team_Batista_4:_Raden_Meikyu';
//            $crawUrl = 'http://asianwiki.com/Hula_Girls';
//            $dbFilmID = 5;
            $filmHtml = @file_get_html($crawUrl);
            if( !$filmHtml) {
                $this->tblFilms->updateCrawlStatus(1,$dbFilmID);
                continue;
            }
            $profileInfo = array();
            $plotInfo = array();
            $noteInfo = "";
            $castInfo = array();
            $episodeRatingInfo = array();
            $image = "";
            $awardInfo = array();
            $imageGalleryInfo = array();
            // get profile
            if (isset($filmHtml->find('#Profile', 0)->plaintext)) {
                $profileInfo = $this->getProfileFromCrawler($filmHtml);
            }

            // get plot
            if (isset($filmHtml->find('#Plot', 0)->plaintext)) {
                $plotHtml = $filmHtml->find('#Plot', 0)->parent()->next_sibling();
                if ($plotHtml->tag == 'p') {
                    $isHavePlot = true;
                    do {
                        $plotInfo [] = strip_tags($plotHtml->outertext,'<ol>,<ul>,<li>,<b>,<p>');

                        $plotHtml = $plotHtml->next_sibling();
                        if ($plotHtml->tag != 'p') {
                            $isHavePlot = false;
                        }
                    } while ($isHavePlot != false);
                }
            }
            // get note
            if (isset($filmHtml->find('#Notes', 0)->plaintext)) {
                $noteHtml = $filmHtml->find('#Notes', 0)->parent()->next_sibling();
                if ($noteHtml->tag == 'ol' || $noteHtml->tag == 'ul') {
                    // removed tag a if exists
                    $noteInfo = strip_tags($noteHtml->outertext, '<ol>,<ul>,<li>,<b><p>');
                }
            }

            // get cast infor
            if (isset($filmHtml->find('#Cast', 0)->plaintext)) {
                // case 1:
                $castHtml = $filmHtml->find('#Cast', 0)->parent()->next_sibling();
                switch ($castHtml->tag) {
                    case 'ul':
                        $castInfo = $this->getActorsFromListUl($castHtml);
                        break;
                    case 'table':
                        // loop to get actor from table
                        $isFoundTable = true;
                        $tableHtml = $castHtml;
                        do {
                            $tmpTagA = array();
                            $tmpExtraInfo = array();
                            foreach ($tableHtml->find('td') as $item) {
                                if (isset($item->find('a', 0)->href) && !isset($item->find('a', 0)->find('img', 0)->src)) {
                                    $tagAHtml = $item->find('a', 0);
                                    $key_href = str_replace('/', '', $tagAHtml->href);
                                    $name = str_replace('/', '', $tagAHtml->plaintext);
                                    if (strpos($key_href, 'index.php') !== false) {
                                        $key_href = str_replace(' ', '_', $name);
                                    }
                                    $tmpTagA[] = ['key_href' => $key_href, 'name' => $name];
                                } else {
                                    $text = trim($item->plaintext);
                                    $text = $this->removeMultipleSpace($text);
                                    if (strlen($text) > 2) {
                                        $tmpExtraInfo[] = $text;
                                    }

                                }
                            }
                            if ($tmpTagA) {
                                foreach ($tmpTagA as $key => $item_info) {
                                    $tmpInfo = "";
                                    if (isset($tmpExtraInfo[$key])) {
                                        $tmpInfo = $tmpExtraInfo[$key];
                                    }
                                    $castInfo[$item_info['key_href']] = ['name' => $item_info['name'], 'extra_info' => $tmpInfo, 'is_additional_cast' => 0];
                                }
                            }
                            $tableHtml = $tableHtml->next_sibling();

                            if ($tableHtml->tag != 'table') {
                                $isFoundTable = false;
                            }

                        } while ($isFoundTable != false);
                        break;
                }
            }


            // get addtional actors
            if (isset($filmHtml->find('p', 0)->plaintext)) {
                foreach ($filmHtml->find('p') as $item) {
                    if (isset($item->find('b', 0)->plaintext)) {
                        $addtionText = $item->find('b', 0)->plaintext;
                        if (strpos($addtionText, 'Additional Cast') !== false) {
                            $addtionCastHtml = $item->find('b', 0)->parent()->next_sibling();
                            if ($addtionCastHtml->tag == 'ul') {
                                $tmpListActors = $this->getActorsFromListUl($addtionCastHtml, 1);
                                if ($tmpListActors) {
                                    $castInfo = array_merge($castInfo, $tmpListActors);
                                }
                            } else if($addtionCastHtml->tag == 'table') {
                                    if(isset($addtionCastHtml->find('ul',0)->plaintext)) {
                                        $tmpListActors = array();
                                        foreach ($addtionCastHtml->find('ul') as $item) {
                                            $tmp = $this->getActorsFromListUl($item,1);
                                            $tmpListActors = array_merge($tmpListActors,$tmp);
                                        }
                                        if ($tmpListActors) {
                                            $castInfo = array_merge($castInfo, $tmpListActors);
                                        }
                                    }

                            }
                        }
                    }
                }

            }
            // get episode rating
            if (isset($filmHtml->find('#Episode_Ratings', 0)->plaintext)) {
                $episodeHtml = $filmHtml->find('#Episode_Ratings', 0)->parent()->next_sibling();
                if ($episodeHtml->tag == 'table') {
                    $episodeRatingInfo = $episodeHtml->outertext;
                }

            }

            // get image
            if (isset($filmHtml->find('.thumbinner .img.thumbimage', 0)->src)) {
                $image = $filmHtml->find('.thumbinner .img.thumbimage', 0)->src;
                if (strpos($image, 'asianwiki.com') === false) {
                    $image = $this->crawlBaseUrl . $image;
                }
            }

            // crawl Awards
            if (isset($filmHtml->find('#Awards', 0)->plaintext)) {
                $awardHtml = $filmHtml->find('#Awards', 0)->parent()->next_sibling();
                if ($awardHtml->tag == 'ul') {
                    $awardInfo = strip_tags($awardHtml->outertext, '<ol>,<ul>,<li>,<b>,<p>');
                }
            }

            // crawl image gallery

            if (isset($filmHtml->find('#slideshow-div .slides ', 0)->plaintext)) {

                foreach ($filmHtml->find('#slideshow-div .slides a') as $item) {
                    if (isset($item->href)) {
                        $imageGalleryInfo[] = $item->href;
                    }
                }
                $imageGalleryInfo = array_unique($imageGalleryInfo);

            }
            $data['profile'] = $profileInfo;
            $data['cast'] = $castInfo;
            $data['award'] = $awardInfo;
            $data['image'] = $image;
            $data['episode_rating'] = $episodeRatingInfo;
            $data['plot'] = $plotInfo;
            $data['note'] = $noteInfo;
            $data['image_gallery'] = $imageGalleryInfo;
            $this->saveDataFromFilmCrawler($data, $dbFilmID);

            $this->tblFilms->updateCrawlStatus(1,$dbFilmID);
		return true;
        }

    }

    protected function isActorPage()
    {

    }

    public function crawlParentLinks()
    {
        $count = 0;
        $listLinks = [
            'women' => 'http://asianwiki.com/Category:Actresses',
            'men' => 'http://asianwiki.com/Category:Actors'

        ];
        $parentLinks = $this->tblLinks->getParentLinks();
        foreach ($listLinks as $gender => $urlCrawl) {
            $html =@file_get_html($urlCrawl);
            if(!$html) {
                continue;
            }
            if (isset($html->find("#mw-content-text .plainlinks a", 0)->plaintext)) {
                foreach ($html->find("#mw-content-text .plainlinks a") as $key => $value) {
                    $link = $value->href;
                    if (!in_array($link, $parentLinks)) {
                        $link = str_replace('&amp;', '&', $link);
                        $this->tblLinks->saveLink(['link' => $link]);
                        $count++;
                    }
                }
            }

        }
        echo "$count links are crawled and inserted into database";
    }

    // crawler section

    public function getFilmFromCrawlerActorPage($html, $selector)
    {
        $films = array();
        $filmHtml = $html->find($selector, 0)->parent()->next_sibling();
        if (isset($filmHtml->tag) && $filmHtml->tag == 'ul') {
            foreach ($filmHtml->find('li') as $item) {
                if (isset($item->find('a', 0)->href)) {
                    $film_href = $item->find('a', 0)->href;
                    $film_href = str_replace('/', '', $film_href);
                    $film_name = trim($item->find('a', 0)->plaintext);
                    $film_name = str_replace('/', '', $film_name);
                    $film_name = preg_replace('/(\s\s+|\t|\n)/', ' ', $film_name);

                    $item->find('a', 0)->innertext = "";
                    $extra_name = trim($item->plaintext);

                    $extra_name = str_replace('|', '', $extra_name);
                    $extra_name = preg_replace('/(\s\s+|\t|\n)/', ' ', $extra_name);
                    $extra_name = trim($extra_name);

                    if (strpos($film_href, 'index.php') !== false) {
                        $film_href = str_replace(' ', '_', $film_name);
                    }
                    $films[$film_href] = ['name' => $film_name, 'extra_info' => $extra_name];

                }
            }
        }
        return $films;

    }

    // update section

    protected function updateCrawledForLink($id)
    {
        $this->tblLinks->updateCrawledForLink($id);
    }

    // check sesction
    protected function isCategoryLink($link)
    {
        if (strpos($link, 'index.php') !== false) {
            return true;
        }
        return false;
    }

    protected function isCategoryLinkExist($data)
    {
        return $this->tblLinks->checkCategoryLinkExist($data);
    }

    protected function isActorExist($data)
    {
        return $this->tblActors->checkActorExist($data);
    }

    protected function isCategoryPageEmpty($html)
    {
        if (isset($html->find('#mw-pages a', 0)->href)) {
            return false;
        }
        return true;
    }

    // filter section

    public function removeMultipleSpace($str)
    {
        $str = preg_replace('/\s+/', ' ', $str);
        return trim($str);
    }

    protected function filterActorName($name)
    {
        $name = trim($name);
        if (strpos($name, '/') !== false) {
            $name = explode('/', $name);
            $name = end($name);
        }
        return $name;
    }

    protected function getFulUrlCrawlLink($url)
    {
        $url = str_replace('&amp;', '&', $url);
        $info = parse_url($url);
        if (isset($info['host'])) {
            return $url;
        }
        return "http://asianwiki.com" . $url;

    }

    protected function getProfileFromCrawler($html)
    {
        $profileHtml = $html->find('#Profile', 0)->parent()->next_sibling();
        $profileInfo = array();
        if ($profileHtml->tag == 'ul') {
            foreach ($profileHtml->find('li') as $item) {
                $profileKey = "";
                $profileName = "";
                if (isset($item->find('text', 1)->plaintext)) {
                    $profileKey = $item->find('text', 1)->plaintext;
                    $profileKey = preg_replace('/(\s\s+|\t|\n)/', ' ', $profileKey);
                    $profileKey = str_replace([' ', ':'], ['_', ''], $profileKey);
                }
                if (isset($item->find('a', 0)->href)) {
                    $profileName = $item->find('b', 0)->innertext = '';
                    $profileName = trim($item->plaintext);
                    $profileName = preg_replace('/(\s\s+|\t|\n)/', ' ', $profileName);

                } else {

                    if (isset($item->find('text', 2)->plaintext)) {
                        $profileName = trim($item->find('text', 2)->plaintext);
                        $profileName = preg_replace('/\s/', ' ', $profileName);
                    }
                }
                $profileInfo[$profileKey] = $profileName;
            }

        }
        return $profileInfo;
    }

    protected function getActorsFromListUl($castHtml, $isAddtionalCast = false)
    {
        $castInfo = array();
        foreach ($castHtml->find('li') as $item) {
            if (isset($item->find('a', 0)->href)) {
                $tagAHtml = $item->find('a', 0);
                $key_href = str_replace('/', '', $tagAHtml->href);
                $name = str_replace('/', '', $tagAHtml->plaintext);
                $name = $this->removeMultipleSpace($name);
                if (strpos($key_href, 'index.php') !== false) {
                    $key_href = str_replace(' ', '_', $name);
                }
                $extra_info = "";
                if (isset($item->find('text', 2)->plaintext)) {
                    $extra_info = trim($item->find('text', 2)->plaintext);
                    $extra_info = ltrim($extra_info, "-");
                    $extra_info = trim($extra_info);
                }

                $castInfo[$key_href] = ['name' => $name, 'extra_info' => $extra_info, 'is_additional_cast' => $isAddtionalCast];
            }
        }
        return $castInfo;
    }

    // save action

    protected function saveDataFromActorCrawler($actorID, $data)
    {
        $this->tblActors->saveActorFromCrawler($actorID, $data);
        $this->tblActorFilms->saveDataFromActorCrawler($actorID, $data);
    }

    protected function saveDataFromFilmCrawler($data, $filmID)
    {
        $this->tblFilms->saveDataFromFilmCrawler($data, $filmID);
        $this->tblActorFilms->saveDataFromFilmCrawler($data, $filmID);
    }

    public function formatUrlKey($str) {
        $urlKey = preg_replace('#[^0-9a-z]+#i', '_', $str);
        $urlKey = strtolower($urlKey);
        return $urlKey;

    }


}
