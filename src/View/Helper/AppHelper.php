<?php
namespace App\View\Helper;
use Cake\View\Helper;

class AppHelper extends  Helper {

    public function minifiedHtml($output) {
        $output = trim(preg_replace('/\r?\n/m', '', $output));
        echo $output;
    }

    public function filterActorImage($img) {

        if ($img) {
            $img = 'https://images2-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&amp;gadget=a&amp;no_expand=1&amp;refresh=604800&amp;url=' . $img;
        } else {
            $img = $this->request->webroot . 'images/default-person.png';
        }
        return $img;
    }

    public function filterFilmImage($img) {

        if ($img) {
            $img = 'https://images2-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&amp;gadget=a&amp;no_expand=1&amp;refresh=604800&amp;url=' . $img;
        } else {
            $img = $this->request->webroot . 'images/default-film.png';
        }
        return $img;
    }
}

