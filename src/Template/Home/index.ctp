<?php $this->assign('metaKeywords', $metaKeywords); ?>
<?php $this->assign('title', $title); ?>
 <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <?php foreach($actors as $item): ?>
                <!-- Listing Item -->
                    <?php
                        $img = $item['avatar'];
                        if ($item['avatar']) {
                            $img = 'https://images2-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&amp;gadget=a&amp;no_expand=1&amp;refresh=604800&amp;url='.$item['avatar'] ;
                        } else {
                            $img = $this->request->webroot .'images/default-person.png';
                        }
                    ?>
                <div class="col-lg-4 col-md-6">
                    <a href="<?php echo $this->request->webroot ?>actor/<?php echo strtolower($item['key_name']); ?>" class="listing-item-container compact">
                        <div class="listing-item">
                            <img src="<?php echo $img; ?>" alt="<?php echo $item['name'] ?> Quotes">
                            <div class="listing-item-content">
                                <h3><?php echo $item['name']; ?></h3>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- Listing Item / End -->
                <?php endforeach; ?>
            </div>
            <!-- Pagination -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- Pagination -->
                    <div class="pagination-container margin-top-20 margin-bottom-40">
                        <nav class="pagination">
                            <?php echo $this->element('pagination'); ?>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- Pagination / End -->

        </div>

    </div>
</div>

