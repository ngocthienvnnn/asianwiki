<?php $this->assign('metaKeywords', $metaKeywords); ?>
<?php $this->assign('title', $title); ?>
<div class="container">
    <!-- Blog Posts -->
    <div class="blog-page">
        <div class="row">

            <!-- Post Content -->
            <div class="col-lg-9 col-md-8 padding-right-30">
                <!-- Blog Post -->
                <div class="blog-post single-post">
                    <!-- Img -->
                    <!--                    <img class="post-img" src="https://images2-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&amp;gadget=a&amp;no_expand=1&amp;refresh=604800&amp;url=-->
                    <?php //echo $subject['Subject']['large_cover'] ?><!--" alt="-->
                    <?php //echo $subject['Subject']['title'] ?><!--">-->

                    <!-- Content -->
                    <div class="post-content">
                        <h1>  <?php echo $actor['name'] ?></h1>
                        <?php $extraInfo = json_decode($actor['info'], true); ?>
                        <?php echo $this->element('social_share', ['share_url' => strtolower($this->Url->build($actor['key_name'], true)), 'share_title' => $actor['name']]) ?>
                        <div class="clearfix"></div>
                        <div style="padding: 10px 0;"></div>

                        <div class="thumbnail" style="float:right;max-width: 252px">
                            <img class="img-responsive"
                                 src="<?php echo $this->App->filterActorImage($actor['avatar']) ?>">
                        </div>

                        <?php if ($extraInfo['profile']) : ?>
                            <div id="profile">
                                <h2> Profile </h2>
                                <ul>
                                    <?php foreach ($extraInfo['profile'] as $key => $item): ?>
                                        <li><b><?php echo str_replace('_', " ", $key) ?>:</b> <?php echo $item; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <?php if (isset($extraInfo['note']) && $extraInfo['note']): ?>
                            <div id="note">
                                <h2> Note </h2>
                                <?php echo $extraInfo['note']; ?>
                            </div>
                        <?php endif; ?>
                        <?php if (isset($extraInfo['biography']) && $extraInfo['biography']): ?>
                            <div id="biography">
                                <h2> Biography </h2>
                                <?php echo $extraInfo['biography']; ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($filmData): ?>
                            <?php foreach ($filmData as $idName => $films): ?>
                                <?php if ($films): ?>
                                    <div id="<?php echo $idName; ?>">
                                        <h2> <?php echo ucwords(str_replace('_', " ", $idName)) ?></h2>
                                        <ul>
                                            <?php foreach ($films as $slug => $item) : ?>
                                                <li>
                                                    <a href="<?php echo $this->request->webroot . 'film/' . strtolower($slug); ?>"><?php echo $item['name'] ?></a> <?php echo $item['extra_info']; ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <div class="clearfix"></div>

                        <?php if (isset($extraInfo['award']) && $extraInfo['award']) : ?>
                            <h2>Awards</h2>
                            <div id="award">
                                <?php echo $extraInfo['award']; ?>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <!-- Blog Post / End -->


                <!-- Post Navigation -->
                <ul id="posts-nav" class="margin-top-0 margin-bottom-45">
                    <li class="next-post">
                        <a href="<?php echo $this->request->webroot ?>actor/<?php echo strtolower($neighbors['next']['key_name']) ?>">
                            <span>Next Post</span>
                            <?php echo $neighbors['next']['name'] ?></a>
                    </li>
                    <li class="prev-post">
                        <a href="<?php echo $this->request->webroot ?>actor/<?php echo strtolower($neighbors['prev']['key_name']) ?>">
                            <span>Previous Post</span>
                            <?php echo $neighbors['prev']['name'] ?></a>
                    </li>
                </ul>

                <!-- Related Posts -->
                <div class="clearfix"></div>
                <h4 class="headline margin-top-25">Related Posts</h4>
                <div class="row">
                    <?php foreach ($relatedActors as $item): ?>
                        <!-- Blog Post Item -->

                        <div class="col-md-6">
                            <a href="<?php echo $this->request->webroot ?>actor/<?php echo strtolower($item['key_name']) ?>"
                               class="blog-compact-item-container">
                                <div class="blog-compact-item">
                                    <img class="img-responsive"
                                         src="<?php echo $this->App->filterActorImage($item['avatar']); ?>"
                                         alt="<?php echo $item['name']; ?>">

                                    <div class="blog-compact-item-content">

                                        <h3><?php echo $item['name'] ?></h3>

                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- Blog post Item / End -->
                    <?php endforeach; ?>

                </div>
                <!-- Related Posts / End -->

            </div>
            <!-- Content / End -->

            <!-- Widgets -->
            <div class="col-lg-3 col-md-4">
                <div class="sidebar right">

                    <!-- Widget -->
                    <div class="widget">
                        <h3 class="margin-top-0 margin-bottom-25">Search Blog</h3>
                        <div class="search-blog-input">
                            <form target="_blank" action="//www.google.com/search" method="GET">
                                <input class="u-full-width" name="q" placeholder="Search" type="text">
                                <input name="sitesearch" value="asiacelebrities.com" type="hidden">
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Widget / End -->

                    <!-- Widget -->
                    <div class="widget margin-top-40">

                        <h3>Popular Posts</h3>
                        <ul class="widget-tabs">
                            <?php ;
                            foreach ($topActors as $item): ?>

                                <!-- Post #1 -->
                                <li>
                                    <div class="widget-content">
                                        <div class="widget-thumb">
                                            <a href="<?php echo $this->request->webroot ?>actor/<?php echo strtolower($item['key_name']); ?>"><img
                                                        class="img-responsive"
                                                        src="<?php echo $this->App->filterActorImage($item['avatar']) ?>"
                                                        alt="<?php echo $item['name'] ?>">
                                            </a>
                                        </div>

                                        <div class="widget-text">
                                            <h5>
                                                <a href="<?php echo $this->request->webroot ?>actor/<?php echo strtolower($item['key_name']) ?>"><?php echo $item['name'] ?></a>
                                            </h5>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                    </div>
                    <!-- Widget / End-->


                    <div class="clearfix"></div>
                    <div class="margin-bottom-40"></div>
                </div>
            </div>
        </div>
        <!-- Sidebar / End -->

    </div>
</div>
