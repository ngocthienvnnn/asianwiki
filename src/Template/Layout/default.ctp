<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title> <?php echo $this->fetch('title'); ?> </title>
    <meta name="keywords" content="<?php echo $this->fetch('metaKeywords') ?>" />
    <meta http-equiv="content-language" content="en" />
    <meta charset="utf-8" />
    <meta name="robots" content="index, follow"/>
    <meta name="author" content="asiacelebrities.com" />
    <meta property="og:type" content="website"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="canonical" href="<?php echo $this->Url->build(null, true)  ?>" />
    <meta property="og:url" itemprop="url" content="<?php echo $this->Url->build(null, true)  ?>" />
    <?php
    echo $this->fetch('meta');
    echo $this->Html->css(array('style','main'), array('inline' => false));
    echo $this->fetch('css');
    ?>
</head>

<body>
<!-- Wrapper -->
<div id="wrapper">
    <?php echo $this->App->minifiedHtml($this->element('header')); ?>

    <div  id="wrapper-content" style="padding: 30px 0;">
        <?php echo $this->App->minifiedHtml($this->fetch('content')); ?>
    </div>

    <?php echo $this->App->minifiedHtml($this->element('footer')); ?>
    <?php echo $this->element('analytics'); ?>
    <?php
    echo $this->Html->script(array('jquery-2.2.0.min.js', 'jpanelmenu.min.js',
        'chosen.min.js', 'slick.min.js', 'rangeslider.min.js',
        'magnific-popup.min.js', 'waypoints.min.js', 'counterup.min.js',
        'jquery-ui.min.js', 'tooltips.min.js', 'custom.js','jquery.slides.min.js'), array('inline' => false));
    echo $this->fetch('script');
    ?>
    <!-- Back To Top Button -->
    <div id="backtotop"><a href="#"></a></div>
</div>
</body>
</html>