<div id="footer" class="">
    <!-- Main -->
    <div class="container">
        <!-- Copyright -->
        <div class="row">
            <div class="col-md-12">
                <div class="copyrights">© 2017 <a href="<?php echo $this->request->webroot ?>">asiacelebrities.com</a>. All Rights Reserved.</div>
            </div>
        </div>

    </div>

</div>