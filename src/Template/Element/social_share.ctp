<?php
echo $this->Html->css(array('likely'), array('inline' => false));
echo $this->Html->script(array('likely'), array('inline' => false));
?>
<div class="clearfix"></div>
<div class="likely" style="padding: 10px 0;" data-url = "<?php echo $share_url; ?>" data-title= "<?php echo $share_title; ?>">
    <div class="facebook">Share</div>
    <div class="twitter">Tweet</div>
    <div class="gplus">+1</div>
    <div class="vkontakte">Share</div>
    <div class="pinterest">Pin</div>
    <div class="odnoklassniki">Like</div>
    <div class="telegram">Send</div>
    <div class="linkedin">Share</div>
</div>
<div class="clearfix"></div>