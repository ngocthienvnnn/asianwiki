<ul class="pagination" role="menubar" aria-label="Pagination">
    <?php echo $this->Paginator->first(); ?>
    <?php echo $this->Paginator->numbers(); ?>
    <?php echo $this->Paginator->last(); ?>
</ul>