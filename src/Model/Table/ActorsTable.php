<?php
    namespace App\Model\Table;
    use Cake\ORM\Table;
    use Cake\Core\Configure;
    use Cake\ORM\TableRegistry;
    class ActorsTable extends Table
    {
        public function checkActorExist($data)
        {
            return $this->exists($data);
        }

        public function saveActor($data)
        {
            $entity = $this->newEntity($data);
            return $this->save($entity);
        }

        public function getActorsNotCrawled($limit)
        {
            return $this->find(
                'all', [
                    'conditions' => ['is_crawled' => 0],
                    'limit' => $limit
                ]
            );
        }

        public function updateCrawlStatus($id, $status)
        {
            $query = $this->query();
            $query->update()
                ->set(['is_crawled' => $status])
                ->where(['id' => $id])
                ->execute();
        }

        /**
         * @param $actorID
         * @param $info
         * @return bool|\Cake\Datasource\EntityInterface|mixed
         */
        public function saveActorFromCrawler($actorID, $info)
        {
            $data['avatar'] = $info['avatar'];
            $data['info']['profile'] = $info['profile'];
            $data['info']['award'] = $info['award'];
            $data['info']['note'] = $info['note'];
            $data['biography'] = $info['biography'];
            $data['info'] = json_encode($data['info']);
            $data['id'] = $actorID;
            $entity = $this->newEntity($data);
            return $this->save($entity);
        }

        public function insertActorIfNotExist($key , $name) {
            $actor = $this->query()->find('all',[
                'conditions' => ['key_name' => $key]
            ])->first();
            if ($actor) {
                return $actor['id'];
            }
            $data['key_name'] = $key;
            $data['name'] = $name;
            $entity = $this->newEntity($data);
            $obj = $this->save($entity);
            return $obj->id;
        }

        public function loadActorsForHomePage($obj) {
            $pagSettings = [
                'limit' => 15,
                'fields' => array('id', 'name', 'avatar','key_name'),


            ];

            $query = $this->find('all',[
                    'order' => [
                        'Actors.gender' => 'DESC',
                        'Actors.avatar' => 'DESC'
                    ]
                ]
                );
            return $obj->paginate($query,$pagSettings);
        }

        public function getActorBySlug($slug) {
            return $this->find('all', [
               'conditions' =>['key_name' => $slug]
            ])->first();
        }

        public function getTopActors() {
            return $this->query()->find('all', [
                'conditions' => ['gender' => 2, 'avatar != ' =>"NULL"],
                'fields' => array('id', 'name', 'avatar','key_name'),
                'limit' => 10
            ]);
        }

        public function getRelatedActors() {
            return $this->query()->find('all', [
                'conditions' => ['gender !=' => 0, 'avatar != ' =>"NULL"],
                'fields' => array('id', 'name', 'avatar','key_name'),
                'order' => ['RAND()'],
                'limit' => 2
            ]);
        }

        public function getNextPrevActors($id) {

            $previous = $this->find()
                ->select(['id','name','key_name'])
                ->order(['id' => 'DESC'])
                ->where(['id <' => $id])
                ->first();
            $next = $this->find()
                ->select(['id','name','key_name'])
                ->order(['id' => 'ASC'])
                ->where(['id >' => $id])
                ->first();
            return ['next' =>$next ,'prev' =>$previous];
        }

        public function getActorsByFilmID($filmID) {
            $result = array();
            $tblActorFilms = TableRegistry::get('ActorsFilms');
            $actorFilms = $tblActorFilms->find('all', [
                'conditions' => ['film_id' => $filmID]
            ]);
            if (!$actorFilms->count()) {
                return false;
            }
            $listActorIds = array();
            $listExtraInfo = array();
            foreach ($actorFilms as $item) {
                $listActorIds[] = $item['actor_id'];
                $listExtraInfo[$item['actor_id']] = ['extra_info' => $item['info_film_page'] , 'is_additional_cast' => $item['is_additional_cast']];
            }
            $listActorIds = array_unique($listActorIds);
            $actors = $this->find('all',[
                'fields' =>['id','avatar','key_name','name'],
                'conditions' => ['id IN' => $listActorIds]
            ]);
            if (!$actors->count()) {
                return false;
            }
            foreach ($actors as $item) {
                if (empty ($item['key_name'])) {
                    continue;
                }
                $typeActor = "main";
                $extraInfo = "";
                $actorID = $item['id'];
                if (isset($listExtraInfo[$actorID]) ) {
                    $typeActor = empty($listExtraInfo[$actorID]['is_addtional_cast']) ? "" : $listExtraInfo[$actorID]['is_addtional_cast'];
                    $extraInfo = $listExtraInfo[$actorID]['extra_info'];
                }
                $item = $item->toArray();
                $item['extra_info'] = $extraInfo;
                $result[$item['key_name']] = $item;
            }
            return $result;
        }
    }