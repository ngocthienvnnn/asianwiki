<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class LinksTable extends Table
{
    public function getParentLinks($is_crawled = 0)
    {
        $results = array();
        $links = $this->find()->where(['parent_id' => 0], ['is_crawled' => $is_crawled])->all();
        if ($links->count()) {
            foreach ($links as $link => $value) {
                $results[$value['id']] = $value['link'];
            }
        }
        return $results;
    }

    public function saveLink($data)
    {
        $linkEntity = $this->newEntity($data);
        return $this->save($linkEntity);
    }

    public function getParentLinksNotActive()
    {
        $links = $this->find()->where(['parent_id' => 0], ['is_crawled' => 1])->all();
    }

    public function updateCrawledForLink($id)
    {
        $query = $this->query();
        $query->update()
            ->set(['is_crawled' => 1])
            ->where(['id' => $id])
            ->execute();
    }

    public function checkCategoryLinkExist($data)
    {
        return $this->exists($data);
    }

    public function getChildCategoryLinkNotActiveByParent($id)
    {
        return $this->query()->find('all', [
            'conditions' => ['parent_id  ' => $id, 'is_crawled' => 0],
            'order' => ['id' => 'ASC']
        ])->first();
    }
}