<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
class FilmsTable extends Table
{
    public function getFilmsNotCrawled($limit) {
        return $this->find(
            'all',[
                'conditions' => ['is_crawled' => 0],
                'limit' => $limit
            ]
        );
    }

    public function updateCrawlStatus($status, $id) {
        $query = $this->query();
        $query->update()
            ->set(['is_crawled' => $status])
            ->where(['id' => $id])
            ->execute();
    }

    public  function getFilmByUrlKey($url_key) {
        return $this->query()->find('all', [
            'conditions' => ['key_name' => $url_key],
        ])->first();
    }

    public function saveFilmFromActorCrawler($urlKey,$info,$filmTypeID) {

        $film =  $this->query()->find('all', [
            'conditions' => ['key_name' => $urlKey],
        ])->first();

        if ($film) {
            if ($film['type']) {
                return $film['id'];
            }
            $data['id'] = $film['id'];
        }
        $data['key_name'] = $urlKey;
        $data['type'] = $filmTypeID;
        $data['name'] = $info['name'];
        $entity = $this->newEntity($data);
        $filmObject = $this->save($entity);
        return $filmObject->id;
    }

    public function saveDataFromFilmCrawler($info,$filmID) {
        $tmp['award'] = $info['award'];
        $tmp['profile'] = $info['profile'];
        $tmp['episode_rating'] = $info['episode_rating'];
        $tmp['note'] = $info['note'];
        $tmp['plot'] = $info['plot'];
        $tmp['image_gallery'] = $info['image_gallery'];
        $data['info'] = json_encode($tmp);
        $data['image'] = $info['image'];
        $data['id'] = $filmID;
        $entity = $this->newEntity($data);
        return $this->save($entity);
    }

    public function getFilmsByActorID($actorID) {
        $filmTypes = Configure::read('FilmTypes');
        $filmResults = array();
        $tblActorFilm = TableRegistry::get('ActorsFilms');
        $actorFilms = $tblActorFilm->find('all',[
            'conditions' => ['actor_id' => $actorID],
            'fields' => ['ActorsFilms.id','ActorsFilms.actor_id','ActorsFilms.film_id','ActorsFilms.info_actor_page','ActorsFilms.is_additional_cast']
        ]);
        if (!$actorFilms->count()) {
            return false;
        }

        $listFilmIDs = array();
        $listFilmExtraInfo = array();
        foreach ($actorFilms as $item) {
           $listFilmIDs[] = $item['film_id'];
            $listFilmExtraInfo[$item['film_id']] = $item['info_actor_page'];
        }
        $listFilmIDs = array_unique($listFilmIDs);
        $objFilms = $this->find('all',[
            'conditions' => ['Films.id IN' => $listFilmIDs],
            'fields' => ['Films.id','Films.key_name','Films.name','Films.type']
        ]);
        foreach ($objFilms as $item) {

            $filmTypeName = array_search($item['type'], $filmTypes);
            $extraInfo = isset($listFilmExtraInfo[$item['id']]) ? $listFilmExtraInfo[$item['id']] : "";
            if (!$filmTypeName) {
                $filmTypeName = 'films';
            }
            $filmResults[$filmTypeName][$item['key_name']] = ['name'=>$item['name'] , 'extra_info' => $extraInfo];
        }
     return $filmResults;
    }

    public function getFilmBySlug($slug) {
        return $this->find('all',[
            'conditions' => ['key_name' => $slug]
        ])->first();
    }

    public function getTopFilms() {
        return $this->query()->find('all', [
            'conditions' => ['image != ' =>"NULL"],
            'fields' => array('id', 'name', 'image','key_name'),
            'limit' => 10
        ]);
    }

    public function getRelatedFilms() {
        return $this->query()->find('all', [
            'conditions' => ['image != ' =>"NULL"],
            'fields' => array('id', 'name', 'image','key_name'),
            'order' => ['RAND()'],
            'limit' => 2
        ]);
    }
    public function getNextPrevFilms($id) {

        $previous = $this->find()
            ->select(['id','name','key_name'])
            ->order(['id' => 'DESC'])
            ->where(['id <' => $id])
            ->first();
        $next = $this->find()
            ->select(['id','name','key_name'])
            ->order(['id' => 'ASC'])
            ->where(['id >' => $id])
            ->first();
        return ['next' =>$next ,'prev' =>$previous];
    }





}