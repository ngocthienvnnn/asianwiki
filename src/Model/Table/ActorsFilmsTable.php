<?php

namespace App\Model\Table;

use Cake\Log\Log;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;
use Cake\Core\Configure;

class ActorsFilmsTable extends Table
{

    public function saveDataFromActorCrawler($actorID, $data)
    {

        $tblFilm = TableRegistry::get('Films');
        $defaultFilmTypes = Configure::read('FilmTypes');
        $filmTypeID = 0;
        foreach ($data as $filmType => $films) {
            switch ($filmType) {
                case 'drama_series' : // type : 2
                case 'movies' : // type : 1
                case 'tv_movies':     // type : 3
                case 'filmography': // type 4
                    if (!$films) {
                        continue;
                    }
                    $filmTypeID = $defaultFilmTypes[$filmType];
                    foreach ($films as $urlKey => $filmData) {

                        $filmID = $tblFilm->saveFilmFromActorCrawler($urlKey, $filmData, $filmTypeID);
                        if (!$filmID) {
                            Log::write('info', '(from crawling actor page)saveFilmFromActorCrawler: ' . $urlKey . ';' . print_r($filmData, true));
                            continue;
                        }
                        $dataInsert = array();
                        $filmActorObject = $this->find('all', [
                            'conditions' => ['actor_id' => $actorID, 'film_id' => $filmID]
                        ])->first();
                        $dataInsert['info_actor_page'] = $filmData['extra_info'];
                        if (!$filmActorObject || ($filmActorObject && empty($filmActorObject['info_actor_page']))) {
                            $dataInsert['actor_id'] = $actorID;
                            $dataInsert['film_id'] = $filmID;
                            $entity = $this->newEntity($dataInsert);
                            $this->save($entity);
                        }
                    }
                    break;
            }
        }
    }

    public function saveDataFromFilmCrawler($info, $filmID)
    {
        $tblActor = TableRegistry::get('Actors');
        $tblFilm = TableRegistry::get('Films');
        if (empty($info['cast'])) {
            return false;
        }
        foreach ($info['cast'] as $key => $item) {
            $data  = array();
            $actorID = $tblActor->insertActorIfNotExist($key, $item['name']);
            $objFilmActor = $this->find('all',[
                'conditions'=>['actor_id' => $actorID , 'film_id' => $filmID]
            ])->first();
            $data['is_additional_cast'] = $item['is_additional_cast'];
            $data['info_film_page'] = $item['extra_info'];
            $data['actor_id'] = $actorID;
            $data['film_id'] = $filmID;
            if ($objFilmActor) {
                $data['id'] = $objFilmActor['id'];
            }

            $entity = $this->newEntity($data);
            $obj = $this->save($entity);
        }
    }

}